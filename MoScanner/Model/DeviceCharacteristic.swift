//
//  DeviceCharacteristic.swift
//  MoScanner
//
//  Created by Ernesto González on 7/2/18.
//  Copyright © 2018 Ernesto González. All rights reserved.
//

import Foundation
import CoreBluetooth

struct DeviceCharateristic {

    var uuid: String
    var accessLevel: [String]
    var value: String?
    var uuidFormatted: String {
        get { return "0x\(uuid)" }
    }

    init(uuid: String, accessLevel: [String], value: String?) {
        self.uuid = uuid
        self.accessLevel = accessLevel
        self.value = value
    }
}
