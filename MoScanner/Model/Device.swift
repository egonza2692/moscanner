//
//  Device.swift
//  MoScanner
//
//  Created by Ernesto González on 7/2/18.
//  Copyright © 2018 Ernesto González. All rights reserved.
//

import Foundation
import CoreBluetooth

struct Device {

    var name: String!
    var manufacturerData: String?
    var uuid: String!
    var characteristic: [DeviceCharateristic]!
    private(set) var peripheral: CBPeripheral!
    private var advertisementData: [String: Any]!

    var isConnectable: Bool {
        guard let connectable = advertisementData[CBAdvertisementDataIsConnectable] as? NSNumber else {
            return false
        }

        return connectable.boolValue
    }

    init(peripheral: CBPeripheral, advertisementData: [String: Any]) {
        self.name = peripheral.name ?? "No Available"
        self.uuid = peripheral.identifier.uuidString
        self.peripheral = peripheral
        self.advertisementData = advertisementData
        self.characteristic = []
        if let manufacturerData = advertisementData[CBAdvertisementDataManufacturerDataKey] as? String {
            self.manufacturerData = manufacturerData
        }
    }
}
