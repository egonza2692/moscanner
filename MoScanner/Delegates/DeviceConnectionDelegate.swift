//
//  DeviceConnectionDelegate.swift
//  MoScanner
//
//  Created by Ernesto González on 7/2/18.
//  Copyright © 2018 Ernesto González. All rights reserved.
//

import Foundation

protocol DeviceConnectionDelegate: class {
    func connectDevice(device: Device)
}
