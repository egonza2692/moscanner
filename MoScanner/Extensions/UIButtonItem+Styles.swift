//
//  UIButtonItem+Styles.swift
//  MoScanner
//
//  Created by Ernesto González on 7/4/18.
//  Copyright © 2018 Ernesto González. All rights reserved.
//

import UIKit

extension UIBarButtonItem {

    func textStyle() {
        self.setTitleTextAttributes(
            [.font: UIFont(name: "Muli", size: 17) ?? UIFont.systemFont(ofSize: CGFloat(17))],
            for: .normal
        )
    }
}
