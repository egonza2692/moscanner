//
//  UINavigationController+Styles.swift
//  MoScanner
//
//  Created by Ernesto González on 7/4/18.
//  Copyright © 2018 Ernesto González. All rights reserved.
//

import UIKit

extension UINavigationController {

    func customStyles() {
        let textStyles: [NSAttributedStringKey: Any] = [
            .font: UIFont(name: "Muli", size: 17) ?? UIFont.systemFont(ofSize: CGFloat(17)),
            .foregroundColor: UIColor.white
        ]
        self.navigationBar.titleTextAttributes = textStyles
        self.navigationBar.barTintColor = UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
        self.navigationBar.tintColor = UIColor.white
    }
}
