//
//  UITableViewExt.swift
//  MoScanner
//
//  Created by Ernesto González on 7/4/18.
//  Copyright © 2018 Ernesto González. All rights reserved.
//

import UIKit

extension UITableView {

    func registerCell<T: UITableViewCell>(_: T.Type) where T: NibLoadableView {
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
}

extension UITableViewCell: ReusableView { }
