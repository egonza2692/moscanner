//
//  DeviceCell.swift
//  MoScanner
//
//  Created by Ernesto González on 7/2/18.
//  Copyright © 2018 Ernesto González. All rights reserved.
//

import UIKit
import CoreBluetooth

class DeviceCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var deviceNameLbl: UILabel!
    @IBOutlet weak var deviceStateLbl: UILabel!
    @IBOutlet weak var connectBtn: ConnectButton!

    var currentDevice: Device!
    weak var delegate: DeviceConnectionDelegate?

    func configureCell(device: Device) {
        self.deviceNameLbl.text = device.name
        self.deviceStateLbl.text = device.isConnectable ? "Connectable" : "Non-Conectable"
        self.connectBtn.isHidden = !device.isConnectable
        currentDevice = device
    }

    @IBAction func connectBtnPressed(_ sender: UIButton) {
        delegate?.connectDevice(device: currentDevice)
    }
}
