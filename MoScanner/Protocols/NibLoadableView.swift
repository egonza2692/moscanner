//
//  NibLoadableView.swift
//  MoScanner
//
//  Created by Ernesto González on 7/2/18.
//  Copyright © 2018 Ernesto González. All rights reserved.
//

import UIKit

protocol NibLoadableView: class {}

extension NibLoadableView where Self: UIView {

    static var nibName: String {
        return String(describing: self)
    }
}
