//
//  ReusableView.swift
//  MoScanner
//
//  Created by Ernesto González on 7/2/18.
//  Copyright © 2018 Ernesto González. All rights reserved.
//

import UIKit

protocol ReusableView: class { }

extension ReusableView where Self: UIView {

    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
