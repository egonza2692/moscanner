# Description
Molekule Technical test

# Technology

## Plataforms supported
- iOS 11

## Devices supported
- iPhone SE
- iPhone 6, 6S, 7, 8 and Plus
- iPhone X

# Getting Started

## Requirements

- iOS 11
- Ruby
- Cocoapods
- Xcode 9.4
- Swift 4.1

## Installation

- Clone git
- Install CocoaPods

### CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```
And then, run the following command:

```bash
$ pod install
```

> CocoaPods 1.1.0+ is required.

## Dependencies (Third party libs, SDKs, APIs, repos, submodules)
- RxSwift
